/* Wickramaarachchi h.p
	Student_no-202092 */
	
#include<stdio.h>

int _display();
//function to calculate variation of attendance with the ticket price
int _no_of_attendance(int price);
//function to calculate the expenditure 
int _expenditure(int price);
//function to calculate the income 
int _income(int price);
//function to calculate the profit
int _profit(int price);


int main()
{
	printf("Relationship between profit and ticket price,\n");
	printf("\n");
	printf("Ticket price   \t Profit \n");
	printf("\n");
	_display();
	printf("\nAccording to the above details,highest profit can be make when ticket price is Rs.25.00");
	
	return 0;
}

int _display()
{
	int profit,price;
	for (price=5;price<=50;price+=5)
	{
		profit=_profit(price);
		printf("Rs.%d .00    --> Rs.%d .00\n",price,profit);
	}
	return 0;
}

int _no_of_attendance(int price)
{
	const int fixed_att=120;
	int attendance;
	if(price>=15)
		attendance=fixed_att-((price-15)/5*20);
	else
		attendance=fixed_att+((15-price)/5*20);
	
	return attendance;
}

int _expenditure(int price)
{
	int cost;
	cost=500+3*_no_of_attendance(price);
	
	return cost;
}

int _income(int price)
{
	int revenue;	
	revenue = _no_of_attendance(price)*price;
	
	return revenue;
}

int _profit(int price)
{
	int profit;
	profit= _income(price)-_expenditure(price);
	
	return profit;
}


